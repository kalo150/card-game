#include <stdio.h>
#include <stdlib.h>
#include "game.h"
#include "card.h"
#include "deck.h"
#include <string.h>
#include "player.h"
#include "board.h"
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_ttf.h"

void start_the_game(int mode, int loaded_deck){
	int count_player_turn[] = {0, 0};
	int player_turn = -1;
	int check;
	char name[20];
	struct player_t players[2];
	system("clear");
	if(!loaded_deck || loaded_deck == 2){
		if(mode){
			printf("Vuvedete ime, s koeto jelaete da igraete: ");
			scanf(" %s", name);
			make_a_player(&players[0], "Bot", NULL);
			if(!loaded_deck){	
				make_a_player(&players[1], name, NULL);
			}else{
				struct deck_t temp_deck;
				char name_of_the_deck[20];
				printf("Napishi ime na testeto ot koeto iskate da zaredite cartite: ");
				scanf("%s", name_of_the_deck);
				while(!open_a_deck(name_of_the_deck, &temp_deck)){
					printf("error: nqma takuv fail\nprobvaite pak: ");
					scanf("%s", name_of_the_deck);
				}
				make_a_player(&players[1], name, &temp_deck);
			}
		}else{
			char name2[20];
			printf("Vuvedete ime, s koeto jelaete da igraete (igrach nomer 1): ");
			scanf(" %s", name);
			make_a_player(&players[0], name, NULL);
			printf("Vuvedete ime, s koeto jelaete da igraete (igrach nomer 2): ");
			scanf(" %s", name2);	
			make_a_player(&players[1], name2, NULL);
		}
	}else{
		if(!load_a_game_to_file(players, &player_turn, &mode))
			return;
	}
	SDL_Window* window = NULL;
	SDL_Renderer* render = NULL;
	SDL_Init(SDL_INIT_VIDEO);
	window = SDL_CreateWindow("Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, GAME_C_RENDERER_W, GAME_C_RENDERER_H, SDL_WINDOW_SHOWN);
	render = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	IMG_Init(IMG_INIT_PNG);
	TTF_Init();
	SDL_SetRenderDrawColor(render, 0xff, 0xff, 0xff, 0xff);
	SDL_RenderClear(render);
	while(1){
		printf("Red na igrach nomer: PLAYER %i",player_turn+1);
		board_print(players[0], players[1], count_player_turn);
		DrawField(render, players, player_turn, DRAW_FIELD_XY_FALSE, 1);
		check = game_start(players, &player_turn, mode, render);
		if(check != 4)
			count_player_turn[player_turn]++;
		if(check && check != 4)
			break;
	}
	TTF_Font* font = TTF_OpenFont("font.ttf", 100); 
	SDL_Rect won = {100, 100, 70, 70};
	SDL_SetRenderDrawColor(render, 0xff, 0xff, 0xff, 0xff);
	SDL_RenderClear(render);
	if(check == 1)
		render_text(render, "PLAYER 1 pobedi", &won, font, 0);
	else if(check == 2)
		render_text(render, "PLAYER 2 pobedi", &won, font, 0);
	else if(check == 3)
		render_text(render, "nqma pobediteli", &won, font, 0);
	won.y = 200;
	render_text(render, "Q for quit", &won, font, 0);
	SDL_RenderPresent(render);
	printf("Q for quit: \n");
	char temp_char;	
	SDL_Event event;
	int quit_event = 0;
	while(!quit_event){
		while(SDL_PollEvent(&event)){
			if(event.type == SDL_MOUSEBUTTONDOWN)
				quit_event = 1;
			if(event.type == SDL_KEYDOWN){
				switch(event.key.keysym.sym){
					case SDLK_q:
						quit_event = 1;
						break;
				}
			}
		}
	}
	TTF_CloseFont(font);
	SDL_DestroyRenderer(render);
	SDL_DestroyWindow(window);
	SDL_Quit();
	IMG_Quit();
	TTF_Quit();
		
}

int game_start(struct player_t *players, int *who_player_turn, int if_singleplayer, SDL_Renderer* render){
	if(check_if_first_time(players, who_player_turn)) return 0;
	int player_turn = *who_player_turn;
	int card = -1, pole = -1;
	if(player_turn || !if_singleplayer){
		int quit_event = 0;
		SDL_Event event;
		printf("Vuvedi '0' za prazen hod, '9' for save\n");
		printf("Vuvedi carta.. \n");
		int check_mouse = 0;
		while(!quit_event){
			while(SDL_PollEvent(&event)){
				if(event.type == SDL_QUIT)
					exit(1);
				if(event.type == SDL_KEYDOWN && !check_mouse){
					if((event.key.keysym.sym >= '0' && event.key.keysym.sym <= '5') || event.key.keysym.sym == '9' || event.key.keysym.sym == SDLK_ESCAPE){
						card = event.key.keysym.sym - '0';
						if(card == 9 || event.key.keysym.sym == SDLK_ESCAPE){
							card = -1;	
							printf("game saved\n");
						}
						quit_event = 1;
						printf("Vuvedi pole.. \n");
					}
				}else if(event.type == SDL_MOUSEBUTTONDOWN){
					if(event.button.button == SDL_BUTTON_LEFT){
						int x = 0, y = 0, mouse_i;
						SDL_GetMouseState(&x,&y);
						if(x > 410){
							card = 0;
							quit_event = 1;
							break;
						}
						int add = 0;
						if(*who_player_turn)
							add = 300;
						if(y > (add+70) && y < (add+160)){
							int mouse_i = 1;
							while(x <= (10*mouse_i+70*(mouse_i-1)) || x >= (70*mouse_i)+10*mouse_i && mouse_i < 5){
								mouse_i++;
							}
							if(mouse_i <= 5){ 
								card = mouse_i;
								check_mouse = 1;
								x = (10*mouse_i+70*(mouse_i-1));
								if(players[*who_player_turn].hand[(mouse_i-1)].health != 0){
									DrawField(render, players, *who_player_turn, x, y, 1);
									y = add+70;
									moving_card(&x, &y, render, players, *who_player_turn);
								}
							}
						}
						if(*who_player_turn)
							add = 100;
						if(y > (170+add) && y < (add+260) && check_mouse){
							mouse_i = 1;
							while(x <= (10*mouse_i+70*(mouse_i-1)) || x >= ((70*mouse_i)+10*mouse_i) && mouse_i < 7){
								mouse_i++;
							}
							if(mouse_i <= 5){
								pole = mouse_i;
								quit_event = 1;
							}
						}								
					}		
				}
			}
		}	
		quit_event = 0;
		if(card && card != -1 && !check_mouse){
			while(!quit_event){
				while(SDL_PollEvent(&event)){
					if(event.type == SDL_KEYDOWN){
						if(event.key.keysym.sym > '0' && event.key.keysym.sym <= '5'){
							pole = event.key.keysym.sym - '0';
							quit_event = 1;
						}
					}
				}
			}
		}
		if((!players[player_turn].hand[card-1].health || players[player_turn].cards_on[pole-1].health) && card && card != -1)
			return 4;
	}else{
		bot(players, &pole, &card);
	}
	if(card == -1){
		save_a_game_to_file(players, *who_player_turn, if_singleplayer);
		return 100;
	}
	if(card){
		pole--; card--;
		if(can_put_card(players[player_turn].hand[card], players[player_turn])){
			players[player_turn].mana -= players[player_turn].hand[card].mana_cost;
			players[player_turn].cards_on[pole] = players[player_turn].hand[card]; 
			players[player_turn].hand[card].health = 0;
			players[player_turn].hand_space--;
			if(players[player_turn].cards_on[pole].health < 0){
				attack_magic(&players[player_turn], &players[!player_turn], players[player_turn].cards_on[pole].power, pole);
			}
		}	
		return 4;	
	}

	for(card = 0; card < 5; card++){
		if(players[player_turn].cards_on[card].health && players[!player_turn].cards_on[card].health)
			attack(&players[player_turn].cards_on[card], &players[!player_turn].cards_on[card]);
		else if(!players[player_turn].cards_on[card].health && players[!player_turn].cards_on[card].health)
			players[player_turn].health -= players[!player_turn].cards_on[card].power;
		else if(players[player_turn].cards_on[card].health && !players[!player_turn].cards_on[card].health)
			players[!player_turn].health -= players[player_turn].cards_on[card].power;
	}
	if(check_if_player_is_dead(players[0], players[1]) == 2)
		return 2;
	else if(check_if_player_is_dead(players[0], players[1]) == 3)
		return 3;
	else if(check_if_player_is_dead(players[0], players[1]) == 1)
		return 1;
	*who_player_turn = !player_turn;
	player_turn = !player_turn;
	turn_begin(&players[player_turn]);
	return 0;		
}

int moving_card(int *x,int *y, SDL_Renderer* render, struct player_t* players, int who_player_turn){
	SDL_Event e;
	SDL_Rect rect = {0, 0, 70, 90};
	SDL_Rect secc = {*x, *y, 70, 90};
	while(1){
		while(SDL_PollEvent(&e)){
			if(e.type == SDL_MOUSEBUTTONUP && e.button.button == SDL_BUTTON_LEFT){
				DrawField(render, players, who_player_turn, *x, *y, 1);
				SDL_GetMouseState(x, y);
				return 0;				
			}	
			DrawField(render, players, who_player_turn, *x, *y, 0);
			SDL_SetRenderDrawColor(render, 0xAA, 0xBB, 0xCC, 0xDD);
			SDL_RenderFillRect(render, &secc);
			SDL_GetMouseState(&rect.x, &rect.y);
			rect.x -= rect.w/2;
			rect.y -= rect.h/2;
			SDL_SetRenderDrawColor(render, 0x00, 0x00, 0xff, 0x00);
			SDL_RenderFillRect(render, &rect);
			SDL_RenderPresent(render);
		}
	}
}

int check_if_first_time(struct player_t *players, int *player_turn){
	int i, z;
	if(*player_turn == -1){
		for(i = 0; i < 2; i++)
			for(z = 0; z < 3; z++){
				players[i].hand[players[i].hand_space] = push_card(&players[i].deck, 1);
				players[i].hand_space++;				
			}
		*player_turn = 0;
		turn_begin(&players[*player_turn]);
		return 1;
	}	
	return 0;	
}

SDL_Texture* makeText(char *text, TTF_Font* font, SDL_Renderer* render){
	SDL_Color color = {0, 0, 0}; 
	SDL_Surface* temp_s = TTF_RenderText_Solid(font, text, color);
	SDL_Texture* temp_t = SDL_CreateTextureFromSurface(render, temp_s);
	SDL_FreeSurface(temp_s);
	return temp_t;
}

char* make_text_from_num(int i, int *w){
	static char temp_arr[10];
	int count = 0;
	temp_arr[count++] = (i % 10) + '0';
	i /= 10;
	while(i){
		temp_arr[count++] = (i % 10) + '0';
		i /= 10;
	}
	temp_arr[count] = '\0';
	int last = (strlen(temp_arr) - 1);
	for(count = 0; count < last; last--, count++){
		char temp = temp_arr[last];
		temp_arr[last] = temp_arr[count];
		temp_arr[count] = temp;
	}
	*w = strlen(temp_arr) * 15;
	return temp_arr;
}

void render_text(SDL_Renderer* render, char *temp, SDL_Rect *top, TTF_Font* font, int change_x){
	top->w = strlen(temp) * 15;
	SDL_Texture* temp2 = makeText(temp, font, render);
	SDL_RenderCopy(render, temp2, NULL, top);
	SDL_DestroyTexture(temp2);
	if(change_x)
		top->x += top->w;
}

char* makeTextFromNum(int i, int delete_c, SDL_Rect* rect){
	static char arr[30];
	static int count = 0;
	char m[2];
	if(delete_c){
		count = 0;
		arr[count] = '\0';
	}
	if(i > 100)
		return arr;
	if(count > 0)
		arr[count++] = '/';	
	if(i < 0)
		arr[count++] = '-';
	if(i < 0 )
		i = i * -1;
	m[0] = '0' + (i/10);
	m[1] = '0' + (i%10);	
	if(i >= 10)
		arr[count++] = m[0];
	arr[count++] = m[1]; 
	arr[count] = '\0';
	if(strlen(arr) <= 7)
		rect->w = 10 * strlen(arr);
	else 
		rect->w = 70;	
	rect->h = 70;

	return arr;	
}

void print_a_player(SDL_Renderer* render, struct player_t player, SDL_Rect top, TTF_Font* font){
	SDL_SetRenderDrawColor(render, 0x00, 0x00, 0xEE, 0xff);
	SDL_RenderFillRect(render, &top);
	if(strlen(player.name) > 6)
		player.name[6] = '\0';
	render_text(render, player.name, &top, font, 0);
	top.x = 75;
	render_text(render, " ", &top, font, 1);
	render_text(render, make_text_from_num(player.health, &top.w), &top, font, 1);
	render_text(render, "hp ", &top, font, 1);
	render_text(render, make_text_from_num(player.mana, &top.w), &top, font, 1);
	render_text(render, "/13mana ", &top, font, 1);
	render_text(render, make_text_from_num(player.deck.top, &top.w), &top, font, 1);
	render_text(render, "deck", &top, font, 1);
}

void DrawField_CoFunction(int z, struct player_t player1, struct player_t player2, int i, SDL_Rect* yx){
	switch(z){
		case 1:
			makeTextFromNum(player1.hand[i].health, 0, yx);
			makeTextFromNum(player1.hand[i].power, 0, yx);
			makeTextFromNum(player1.hand[i].mana_cost, 0, yx);
			break;
		case 2:
			makeTextFromNum(player1.cards_on[i].health, 0, yx);
			makeTextFromNum(player1.cards_on[i].power, 0, yx);
			break;
		case 3:
			makeTextFromNum(player2.cards_on[i].health, 0, yx);
			makeTextFromNum(player2.cards_on[i].power, 0, yx);
			break;
		case 4:
			makeTextFromNum(player2.hand[i].health, 0, yx);
			makeTextFromNum(player2.hand[i].power, 0, yx);
			makeTextFromNum(player2.hand[i].mana_cost, 0, yx);
			break;
	}
}

void DrawField(SDL_Renderer* render, struct player_t *player, int player_turn, int x, int y, int present){
	int i, l, cycle;
	TTF_Font* font = TTF_OpenFont("font.ttf", 100);
	SDL_Rect yx = {10, 10, 70, 90};
	SDL_Rect top = {0, 0, 410, 60};
	SDL_Rect player_turn_rect = {(GAME_C_RENDERER_W - 45), 0, 45, GAME_C_RENDERER_H}; 
	yx.y = 10;
	SDL_SetRenderDrawColor(render, 0xff, 0xff, 0xff, 0xff);	
	SDL_RenderClear(render);	
	print_a_player(render, player[0], top, font);	
	yx.y += 60;
	for(cycle = 1; cycle <= 4; cycle++){
		for(i = 0; i < 5; i++){
			if(cycle == 2 || cycle == 3)
				SDL_SetRenderDrawColor(render, 0x00, 0xff, 0xff, 0xff);
			else
				SDL_SetRenderDrawColor(render, 0x00, 0xff, 0x00, 0xff);
			yx.w = 70;
			yx.h = 90;
			SDL_RenderFillRect(render, &yx);
			if((player[(cycle/3)].cards_on[i].health) && (cycle == 2 || cycle == 3) || (player[(cycle/3)].hand[i].health) && (cycle == 1 || cycle == 4)){
				makeTextFromNum(101, 1, &yx);
				DrawField_CoFunction(cycle, player[0], player[1], i, &yx);
				SDL_Texture* temp = makeText(makeTextFromNum(101, 0, &yx), font, render);
				SDL_RenderCopy(render, temp, NULL, &yx);
			}
			yx.x += 80;
		}
		yx.y += 100;
		yx.x = 10;
	}	
	SDL_SetRenderDrawColor(render, 0x00, 0x00, 0xEE, 0xff);
	top.y = yx.y; 
	top.x = 0;
	top.w = 410;
	player_turn_rect.y = yx.y;
	print_a_player(render, player[1], top, font);
	player_turn_rect.y = 0;
	SDL_SetRenderDrawColor(render, 0x00, 0xBB, 0x00, 0xCC);
	SDL_RenderFillRect(render, &player_turn_rect);
	player_turn_rect.y = yx.y;
	player_turn_rect.h = 60;
	if(!player_turn)
		player_turn_rect.y = 0;
	SDL_SetRenderDrawColor(render, 0xEE, 0x00, 0xBB, 0xAA);
	SDL_RenderFillRect(render, &player_turn_rect);	
	if(y > 200) y = 370; else if(y < 200 && y > 0) y = 70;
	SDL_Rect temp = {x, y, 40, 10};
	SDL_SetRenderDrawColor(render, 0xff, 0x00, 0xFF, 0xFF);
	SDL_RenderFillRect(render, &temp);	
	if(present)
		SDL_RenderPresent(render);
	TTF_CloseFont(font);
}

