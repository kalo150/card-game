#include "card.h"

int attack(struct card_t *attacker, struct card_t *defender){
	//Vrushta '2' ako attacker e pobedil i '1' ako defender

	if((attacker->health -= defender->power) <= 0)
		attacker->health = 0;

	if((defender->health -= attacker->power) <= 0)
		defender->health = 0;
	
	if(attacker->health && defender->health)
		return 0;
	if(attacker->health && !defender->health)
		return 1;
	if(!attacker->health && defender->health)
		return 2;
	return 3;
}
