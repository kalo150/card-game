#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED
#include "SDL2/SDL_ttf.h"
#include "SDL2/SDL.h"

SDL_Texture* make_font(TTF_Font*, char *name, SDL_Renderer*);
int game_menu_f();

#endif
