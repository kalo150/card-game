#include "deck.h"
#include <stdio.h>
#include <time.h> 
#include <string.h>
#include "card.h"

int make_a_deck(struct deck_t* deck, int cards){
	static int z = 10;
	z += z;
	srand(time(NULL)+z);
	int i = 0, m;
	deck->top = cards-1;
	for(; i < cards; i++){
		if(rand()%5){
			strcpy(deck->cards[i].name, "Melee");
			deck->cards[i].health = rand() % 10+2;
			deck->cards[i].power = deck->cards[i].health / ((rand()% 18 + 10)/ 10);
			deck->cards[i].mana_cost = ((deck->cards[i].health+deck->cards[i].power)/2) / ((rand()%1+10)/10);
		}else{
			strcpy(deck->cards[i].name, "Magic");
			deck->cards[i].health = -1;
			deck->cards[i].power = rand() % 5 + 1;
			deck->cards[i].mana_cost = 1;
		}
	}
	return 1;
}

int open_a_deck(char *name, struct deck_t* deck){
	
	FILE *temp_file = fopen(name, "r");
	if(temp_file == NULL)
		return 0;
	if(name[(strlen(name)-3)] != 'c' || name[(strlen(name)-2)] != 's' || name[(strlen(name)-1)] != 'v')
		return 0;
	int i = 0;
	for(; i < 30; i++)
		fscanf(temp_file, "%s , %i , %i , %i\n", deck->cards[i].name, &deck->cards[i].health, &deck->cards[i].power, &deck->cards[i].mana_cost);
	fclose(temp_file);
	deck->top = 29;
	return 1;
}

int save_a_deck_to_file(char *name, int cards, struct deck_t* decks){
	FILE *file = fopen(name, "w");
	int i = 0;	
	if(decks == NULL){
		struct deck_t deck;
		make_a_deck(&deck, cards);
		for(; i < cards; i++)
			fprintf(file, "%s , %i , %i , %i\n", deck.cards[i].name, deck.cards[i].health, deck.cards[i].power, deck.cards[i].mana_cost);
	}else{
		for(; i < cards; i++)
			fprintf(file, "%s , %i , %i , %i\n", decks->cards[i].name, decks->cards[i].health, decks->cards[i].power, decks->cards[i].mana_cost);
	}
	fclose(file);
	return 1;
}

struct card_t push_card(struct deck_t* deck, int change_top){
	struct card_t card={0};
	if((deck->top)==-1){
		return card;
	}
	card = deck->cards[deck->top];
	if(change_top)
		deck->top--;
	return card;	
}

void mix_a_deck(struct deck_t* deck, int option){
		if(option == MIX_RANDOM_DECK){
		struct deck_t deck2;
		int i, index;
		for(i = 0; i <= deck->top; i++)
			mix_a_deck_copycards(&deck2.cards[i], &deck->cards[i]);
		srand(time(NULL));
		i = deck->top;
		for(; i >= 0; i--){
			index = rand()%(i+1);
			mix_a_deck_copycards(&deck->cards[i], &deck2.cards[index]);
			mix_a_deck_copycards(&deck2.cards[index], &deck2.cards[i]);
		}
	}else if(option == MIX_PRICEMANA_DECK){
		int i = 0, z = 0;
		for(; i <= deck->top; i++)
			for(z = 0; z <= deck->top; z++)
				if(deck->cards[i].mana_cost > deck->cards[z].mana_cost){
					struct card_t temp_card_t;
					mix_a_deck_copycards(&temp_card_t, &deck->cards[z]);	
					mix_a_deck_copycards(&deck->cards[z], &deck->cards[i]);		
					mix_a_deck_copycards(&deck->cards[i], &temp_card_t);			
				}
	}else if(option == MIX_HEALTH_DECK){
		int i = 0, z = 0;
		for(; i <= deck->top; i++)
			for(z = 0; z <= deck->top; z++)
				if(deck->cards[i].health > deck->cards[z].health){
					struct card_t temp_card_t;
					mix_a_deck_copycards(&temp_card_t, &deck->cards[z]);	
					mix_a_deck_copycards(&deck->cards[z], &deck->cards[i]);		
					mix_a_deck_copycards(&deck->cards[i], &temp_card_t);			
				}		
	}else if(option == MIX_ATTACK_DECK){
		int i = 0, z = 0;
		for(; i <= deck->top; i++)
			for(z = 0; z <= deck->top; z++)
				if(deck->cards[i].power > deck->cards[z].power){
					struct card_t temp_card_t;
					mix_a_deck_copycards(&temp_card_t, &deck->cards[z]);	
					mix_a_deck_copycards(&deck->cards[z], &deck->cards[i]);		
					mix_a_deck_copycards(&deck->cards[i], &temp_card_t);			
				}
	}else if(option == MIX_RPICEATTACK_DECK){
		int i = 0, z = 0;
		mix_a_deck(deck, MIX_PRICEMANA_DECK);
		for(; i <= deck->top; i++)
			for(z = 0; z <= deck->top; z++)
				if(deck->cards[i].mana_cost == deck->cards[z].mana_cost)
					if(deck->cards[i].power > deck->cards[z].power){
						struct card_t temp_card_t;
						mix_a_deck_copycards(&temp_card_t, &deck->cards[z]);	
						mix_a_deck_copycards(&deck->cards[z], &deck->cards[i]);		
						mix_a_deck_copycards(&deck->cards[i], &temp_card_t);
					}			
				
	}
	save_a_deck_to_file("mixed.csv", (deck->top+1), deck);
}

void mix_a_deck_copycards(struct card_t* card, struct card_t* card2){
	card->health = card2->health;
	card->power = card2->power;
	card->mana_cost = card2->mana_cost;
	strcpy(card->name, card2->name);
}

