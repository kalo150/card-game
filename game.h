#ifndef GAME_H_FILE
#define GAME_H_FILE
#define GAME_C_RENDERER_W 450
#define GAME_C_RENDERER_H 530
#define DRAW_FIELD_XY_FALSE -100, -100
#include "card.h"
#include "deck.h"
#include "player.h"
#include "board.h"
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_ttf.h"

void start_the_game(int, int);
char* make_text_from_num(int, int*);
int check_if_first_time(struct player_t*, int*);
int game_start(struct player_t*, int*, int, SDL_Renderer*);
SDL_Texture* makeText(char *text, TTF_Font* font, SDL_Renderer*);
char* makeTextFromNum(int, int, SDL_Rect*);
void DrawField(SDL_Renderer*, struct player_t*, int, int, int, int);
void DrawField_CoFunction(int, struct player_t, struct player_t, int, SDL_Rect*);
void render_text(SDL_Renderer*, char *, SDL_Rect *, TTF_Font*, int);
void print_a_player(SDL_Renderer*, struct player_t, SDL_Rect, TTF_Font*);
int moving_card(int*, int*, SDL_Renderer*, struct player_t*, int);

#endif
