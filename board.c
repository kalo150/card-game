#include "board.h"
#include "player.h"
#include <stdio.h>

void board_print(struct player_t player1, struct player_t player2, int *count){
system("clear");
int i;
printf("\nPLAYER 1: %s (%ihp), MANA %i/13, DECK:%i, TURN: %i\n", player1.name, player1.health, player1.mana, player1.deck.top+1, count[0]);
printf(" _____________________________________________________________________\n|");
for(i=0;i<5;i++){
	printf("    ");
	if(player1.hand[i].health) 
		printf("%i|%i(%i)   |", player1.hand[i].health, player1.hand[i].power, player1.hand[i].mana_cost); 
	else
		printf("         |");
}
printf("\n #####################################################################\n#");
for(i=0;i<5;i++){
	printf("     ");
	if(player1.cards_on[i].health) 
		printf("%i|%i     #", player1.cards_on[i].health, player1.cards_on[i].power); 
	else
		printf("        #");
}
printf("\n#");
for(i=0;i<5;i++){
	printf("     ");
	if(player2.cards_on[i].health) 
		printf("%i|%i     #", player2.cards_on[i].health, player2.cards_on[i].power); 
	else
		printf("        #");
}
printf("\n #####################################################################\n|");
for(i=0;i<5;i++){
	printf("    ");
	if(player2.hand[i].health) 
		printf("%i|%i(%i)   |", player2.hand[i].health, player2.hand[i].power, player2.hand[i].mana_cost); 
	else
		printf("         |");
}
printf("\n _____________________________________________________________________\n");
printf("PLAYER 2: %s (%ihp), MANA %i/13, DECK: %i, TURN: %i\n\n", player2.name, player2.health, player2.mana, player2.deck.top+1, count[1]);
printf("1 - dava 5 hp\n2 - maha 5 hp\n3 - dava 3 mana\n4 - krade 2 hp\n5 - prevrushta karta v koza\n");
}
