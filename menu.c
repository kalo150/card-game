#include <stdio.h>
#include "SDL2/SDL.h"
#include "menu.h"
#include "game.h"
#include "deck.h"
#include "SDL2/SDL_ttf.h"
#include "SDL2/SDL_image.h"

SDL_Texture* make_font(TTF_Font *font, char *name, SDL_Renderer* render){
	SDL_Color color = {0, 0, 0};
	SDL_Surface* temp_s = TTF_RenderText_Solid(font, name, color);
	SDL_Texture* temp_t = SDL_CreateTextureFromSurface(render, temp_s);
	SDL_FreeSurface(temp_s);
	return temp_t;
}

int game_menu_f(){

	TTF_Font* font = NULL;
	SDL_Window* window = NULL;
	SDL_Renderer* render = NULL;
	SDL_Init(SDL_INIT_VIDEO);
	window = SDL_CreateWindow("Menu", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 120, 350, SDL_WINDOW_SHOWN);	
	render = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	TTF_Init();
	IMG_Init(IMG_INIT_PNG);
	font = TTF_OpenFont("font.ttf", 30);
	SDL_Texture* menu_options[] = {make_font(font, "Singleplayer", render), make_font(font, "Multiplayer", render), make_font(font, "Resume", render), make_font(font, "Make a deck", render), make_font(font, "Load a deck", render), make_font(font, "Razburkai", render), make_font(font, "Exit", render)};
	SDL_Rect yx = {0, 0, 120, 50}; 
	SDL_SetRenderDrawColor(render, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(render);
	int i, max = sizeof(menu_options)/sizeof(SDL_Texture*);
	SDL_SetRenderDrawColor(render, 0x00, 0xFF, 0xFF, 0xFF);
	SDL_RenderFillRect(render, &yx);
	for(i = 0; i < max; i++){
		yx.y = i * 50;
		SDL_RenderCopy(render, menu_options[i], NULL, &yx);
	}	
	SDL_RenderPresent(render);	
	int quit = 0, pick = -1, current_max = 0, x, y;
	yx.y = 0;
	SDL_Event event;
	while(!quit){
		while(SDL_PollEvent(&event)){
			switch(event.type){
				case SDL_QUIT:
					current_max = max-1;
					quit = 1;
					break;
				case SDL_KEYDOWN:
					switch(event.key.keysym.sym){
						case SDLK_RETURN:
							quit = 1;
							break;
						case SDLK_DOWN:
							if(current_max < (max-1))
								current_max++;
							break;
						case SDLK_UP:
							if(current_max > 0)
								current_max--;
							break;
					}
					SDL_SetRenderDrawColor(render, 0xFF, 0xFF, 0xFF, 0xFF);
					SDL_RenderClear(render);
					yx.y = current_max* 50;
					SDL_SetRenderDrawColor(render, 0x00, 0xFF, 0xFF, 0xFF);
					SDL_RenderFillRect(render, &yx);
					for(i = 0, yx.y = 0; i < max; i++){
						yx.y = 50 * i;
						SDL_RenderCopy(render, menu_options[i], NULL, &yx);
					}
					SDL_RenderPresent(render);
					break;
				case SDL_MOUSEBUTTONUP:
					if(event.button.button == SDL_BUTTON_LEFT){
						SDL_GetMouseState(&x, &y);
						for(i = 0; i < max; i++){
							if(y >= (i * 50) && y < ((i+1)*50)){
								current_max = i;
								SDL_SetRenderDrawColor(render, 0xFF, 0xFF, 0xFF, 0xFF);
								SDL_RenderClear(render);
								SDL_SetRenderDrawColor(render, 0x00, 0xFF, 0xFF, 0xFF);
								yx.y = 50 * i;
								SDL_RenderFillRect(render, &yx);
								for(i = 0, yx.y = 0; i < max; i++){
									yx.y = 50 * i;
									SDL_RenderCopy(render, menu_options[i], NULL, &yx);
								}	
								SDL_RenderPresent(render);
								quit = 1;			
								i = max;
							}
						}
					}
					break;

			}
		}
	}
	SDL_DestroyRenderer(render);	
	SDL_DestroyWindow(window);
	TTF_CloseFont(font);
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
	if(current_max == 0){
		start_the_game(1, 0);	
	}else if(current_max == 1){
		start_the_game(0, 0);
	}else if(current_max == 2){
		start_the_game(0, 1);
	}else if(current_max == 3){
		save_a_deck_to_file("data_card.csv", 30, NULL);
	}else if(current_max == 4){
		start_the_game(1, 2);
	}else if(current_max == 5){
		system("clear");
		printf("mix by:\n1 -> random\n2 -> price mana\n3 -> health\n4 -> attack\n5 -> price and then attack\npick something: ");
		int choise;
		scanf("%i", &choise);
		choise -= 1;
		struct deck_t deck;
		open_a_deck("data_card.csv", &deck);
		mix_a_deck(&deck, choise);
	}else if(current_max == 6){
		return 0;
	}
	return 1;
}

