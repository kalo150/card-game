#ifndef DECK_H_FILE
#define DECK_H_FILE
#include "card.h"

struct deck_t{
struct card_t cards[30];
int top;
};

enum{
	MIX_RANDOM_DECK,
	MIX_PRICEMANA_DECK,
	MIX_HEALTH_DECK,
	MIX_ATTACK_DECK,
	MIX_RPICEATTACK_DECK
};

int make_a_deck(struct deck_t*, int);
struct card_t push_card(struct deck_t*, int); 
int open_a_deck(char *, struct deck_t*);
int save_a_deck_to_file(char *, int, struct deck_t*);
void mix_a_deck(struct deck_t*, int);
void mix_a_deck_copycards(struct card_t*, struct card_t*);

#endif
