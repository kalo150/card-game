#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "player.h"
#include "deck.h"

int can_put_card(struct card_t card, struct player_t player){
	
	if((player.mana-card.mana_cost) >= 0)
		return 1;
	return 0;

}

void turn_begin(struct player_t *player){
	if(player->hand_space < 5){
		int i = 0;
		while(player->hand[i].health) i++;
		player->hand[i] = push_card(&player->deck, 1);
		player->hand_space++;
	}
	if(player->mana < 13)
		player->mana++;
}

void make_a_player(struct player_t *player, char *player_name, struct deck_t *deck){
	if(deck == NULL)
		make_a_deck(&player->deck, 30);
	else
		player->deck = *deck;	
	int i = 0;
	strcpy(player->name, player_name);
	for(; i < 5; i++){
		strcpy(player->hand[i].name, "Empty");
		player->hand[i].health = 0;
		player->hand[i].power = 0;
		player->hand[i].mana_cost = 0;
		strcpy(player->cards_on[i].name, "Empty");
		player->cards_on[i].health = 0;
		player->cards_on[i].power = 0;
		player->cards_on[i].mana_cost = 0;		
	}
	player->health = 30;
	player->mana = 1;
	player->hand_space = 0;
}

int check_if_player_is_dead(struct player_t player1, struct player_t player2){
	if(player1.health <= 0 && player2.health <= 0)
		return 3;
	if(player1.health <= 0)
		return 2;
	if(player2.health <= 0)
		return 1;
	return 0;
}

int bot(struct player_t *players, int *pole, int *card){
	sleep(1);
	int i, z = 0, bot_turn = 0, y;
	int space[2] = {0};
	struct available arr[5] = {0, 0};
	struct available enemy[5] = {0, 0};
	for(i = 0;i < 5; i++){
		if(can_put_card(players[bot_turn].hand[i], players[bot_turn]) && players[bot_turn].hand[i].health){
			arr[z].cards = players[bot_turn].hand[i];
			arr[z].number_deck = i;
			z++;
		}
		if(!players[bot_turn].cards_on[i].health)
			space[bot_turn]++;
		if(players[!bot_turn].cards_on[i].health){
			enemy[space[!bot_turn]].cards = players[!bot_turn].cards_on[i];
			enemy[space[!bot_turn]].number_deck = i;
			space[!bot_turn]++;					
		}
	}
	if(!z){
		*pole = 0;
		*card = 0;
		return 1;
	}
	for(i = 0; i < z; i++)
		for(y = 0; y < z; y++)
			if(arr[y].cards.mana_cost > arr[i].cards.mana_cost){
				struct available temp = arr[y];
				arr[y] = arr[i];
				arr[i] = temp;
			}else if(arr[y].cards.mana_cost == arr[i].cards.mana_cost){
				if(arr[y].cards.health >= arr[i].cards.health && i < y){
					struct available temp = arr[y];
					arr[y] = arr[i];
					arr[i] = temp;
				}				
			}
	for(i = 0; i < space[!bot_turn]; i++){
		if(!players[bot_turn].cards_on[enemy[i].number_deck].health){
			*pole = enemy[i].number_deck+1;
			*card = arr[z-1].number_deck+1;
			return 1;			
		}
	}
	for(i = 0; i < 5; i++){
		if(!players[bot_turn].cards_on[i].health){
			*pole = i+1;
			*card = arr[z-1].number_deck+1;
			return 1;			
		}		
	}
	*pole = 0;
	*card = 0;
	return 1;

}


int attack_magic(struct player_t *attacker, struct player_t *defender, int i, int m){
	switch(i){
		int n, max = 0;
		case 1:
			attacker->health += 5;
			break;
		case 2:
			defender->health -= 5;
			break;
		case 3:
			attacker->mana += 3;
			break;
		case 4:
			attacker->health += 2;
			defender->health -= 2;
			break;
		case 5:
			if(defender->cards_on[m].health > 0){
				defender->cards_on[m].health = 1;
				defender->cards_on[m].power = 1;
			}else{
				for(n = 1; n < 5; n++){
					if(defender->cards_on[n].power > defender->cards_on[max].power)
						max = n;						
				}
			}
			if(defender->cards_on[max].health){
				defender->cards_on[max].health = 1;
				defender->cards_on[max].power = 1;
			}
			break;
	}
	attacker->cards_on[m].health = 0;	
	return 1;
}

void save_a_game_to_file(struct player_t *players, int who_player_turn, int if_singleplayer){
	FILE *file = fopen("save_data.txt", "w");
	int i = 0, z = 0;
	fprintf(file, "%i , %i\n", who_player_turn, if_singleplayer);	
	for(; z < 2; z++){
		fprintf(file, "%i , %i , %i , %i , %s\n", players[z].deck.top, players[z].health, players[z].mana, players[z].hand_space, players[z].name);	
		for(i = 0 ; i <= players[z].deck.top; i++)
			fprintf(file, "%s , %i , %i , %i\n", players[z].deck.cards[i].name, players[z].deck.cards[i].health, players[z].deck.cards[i].power, players[z].deck.cards[i].mana_cost);
		for(i = 0; i < 5; i++){
			fprintf(file, "%s , %i , %i , %i\n", players[z].hand[i].name, players[z].hand[i].health, players[z].hand[i].power, players[z].hand[i].mana_cost);
			fprintf(file, "%s , %i , %i , %i\n", players[z].cards_on[i].name, players[z].cards_on[i].health, players[z].cards_on[i].power, players[z].cards_on[i].mana_cost);
		}
	}	
	fclose(file);
}

int load_a_game_to_file(struct player_t *players, int* who_player_turn, int* if_singleplayer){
	FILE *file = fopen("save_data.txt", "r");
	if(file == NULL)
		return 0;
	int i = 0, z = 0;
	fscanf(file, "%i , %i\n", who_player_turn, if_singleplayer);	
	for(; z < 2; z++){
		fscanf(file, "%i , %i , %i , %i , %s\n", &players[z].deck.top, &players[z].health, &players[z].mana, &players[z].hand_space, players[z].name);	
		for(i = 0 ; i <= players[z].deck.top; i++)
			fscanf(file, "%s , %i , %i , %i\n", players[z].deck.cards[i].name, &players[z].deck.cards[i].health, &players[z].deck.cards[i].power, &players[z].deck.cards[i].mana_cost);
		for(i = 0; i < 5; i++){
			fscanf(file, "%s , %i , %i , %i\n", players[z].hand[i].name, &players[z].hand[i].health, &players[z].hand[i].power, &players[z].hand[i].mana_cost);
			fscanf(file, "%s , %i , %i , %i\n", players[z].cards_on[i].name, &players[z].cards_on[i].health, &players[z].cards_on[i].power, &players[z].cards_on[i].mana_cost);
		}
	}	

	fclose(file);	
	return 1;
}
