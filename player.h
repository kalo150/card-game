#ifndef PLAYER_H_FILE
#define PLAYER_H_FILE
#include "deck.h"

struct available{
	struct card_t cards;
	int number_deck;
};

struct player_t{
char name[10];
struct deck_t deck;
struct card_t hand[5];
struct card_t cards_on[5];
int health, mana, hand_space;
};


int attack_magic(struct player_t*, struct player_t*, int, int);
int bot(struct player_t*, int*, int*);
int check_if_player_is_dead(struct player_t, struct player_t);
void make_a_player(struct player_t*, char*, struct deck_t*);
int change_hp(struct player_t*, int);
int can_put_card(struct card_t, struct player_t);
void turn_begin(struct player_t *);
void save_a_game_to_file(struct player_t *players, int, int);
int load_a_game_to_file(struct player_t *players, int*, int*);

#endif
